package sequential;

import utils.Filters;
import utils.Utils;

import java.awt.*;
import java.util.List;

import static utils.FiltersMethods.CLEAN_IMAGE_FILTER_SEQUENTIAL;
import static utils.FiltersMethods.HIGHLIGHT_FIRE_FILTER_SEQUENTIAL;
import static utils.Utils.logExecutionTimeOfMethod;
import static utils.Utils.saveExecutionDetails;

public class FiltersSequential implements Filters {

    List<String> files;
    List<Color[][]> images;

    // Constructor with filename for source image
    FiltersSequential(List<String> filenames) {
        this.files = filenames;
        images = Utils.loadImages(filenames);
    }

    // Highlight Fires.
    public void highLightFireFilter(String outputFile, float threshold) {
        long startTime = System.currentTimeMillis();

        Color[][] copyImage = Utils.copyImage(images.get(0));

        for (int i = 0; i < copyImage.length; i++) {
            for (int j = 0; j < copyImage[0].length; j++) {
                // fetches values of each pixel
                Color pixel = copyImage[i][j];
                int r = pixel.getRed();
                int g = pixel.getGreen();
                int b = pixel.getBlue();
                // takes average of color values
                int avg = (r + g + b) / 3;
                if (r > avg * threshold && g < 100 && b < 200)
                    // outputs red pixel
                    copyImage[i][j] = new Color(255, 0, 0);
                else
                    copyImage[i][j] = new Color(avg, avg, avg);

            }
        }
        Utils.writeImage(copyImage, outputFile);

        long endTime = System.currentTimeMillis();
        logExecutionTimeOfMethod(startTime, endTime, HIGHLIGHT_FIRE_FILTER_SEQUENTIAL.name());
        saveExecutionDetails(endTime - startTime,
                HIGHLIGHT_FIRE_FILTER_SEQUENTIAL.name() + " " + Utils.dateFormat.format(startTime), 1, images);
    }

    //Clean Image
    public void cleanImageFilter(String outputFile) {
        long startTime = System.currentTimeMillis();

        Color[][] copyImage = Utils.copyImage(images.get(0));

        for (int i = 0; i < copyImage.length; i++) {
            for (int j = 0; j < copyImage[0].length; j++) {
                double sumRed = 0;
                double sumGreen = 0;
                double sumBlue = 0;

                for (Color[][] image : images) {
                    // fetches values of each pixel
                    Color pixel = image[i][j];
                    double r = pixel.getRed();
                    double g = pixel.getGreen();
                    double b = pixel.getBlue();

                    sumRed += r;
                    sumGreen += g;
                    sumBlue += b;
                }

                double avgRed = sumRed / images.size();
                double avgGreen = sumGreen / images.size();
                double avgBlue = sumBlue / images.size();

                double lowestDistance = Double.MAX_VALUE;

                for (Color[][] image : images) {
                    Color pixel = image[i][j];
                    double r = pixel.getRed();
                    double g = pixel.getGreen();
                    double b = pixel.getBlue();

                    double d = Math.sqrt(Math.pow((avgRed - r), 2) +
                            Math.pow((avgGreen - g), 2) +
                            Math.pow((avgBlue - b), 2));

                    if (d <= lowestDistance) {
                        lowestDistance = d;
                        copyImage[i][j] = pixel;
                    }
                }
            }
        }

        Utils.writeImage(copyImage, outputFile);

        long endTime = System.currentTimeMillis();
        logExecutionTimeOfMethod(startTime, endTime, CLEAN_IMAGE_FILTER_SEQUENTIAL.name());
        saveExecutionDetails(endTime - startTime,
                CLEAN_IMAGE_FILTER_SEQUENTIAL.name() + " " + Utils.dateFormat.format(startTime), 1, images);
    }
}

package multithread;

import utils.Filters;
import utils.Utils;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static utils.FiltersMethods.CLEAN_IMAGE_FILTER_MULTITHREADED;
import static utils.FiltersMethods.HIGHLIGHT_FIRE_FILTER_MULTITHREADED;
import static utils.Utils.logExecutionTimeOfMethod;
import static utils.Utils.saveExecutionDetails;

public class FiltersMultithreaded implements Filters {

    List<String> files;
    int numberOfThreads;
    List<Color[][]> images;
    List<Thread> threads;

    // Constructor with filename for source image
    FiltersMultithreaded(List<String> filenames, int numberOfThreads) {
        this.files = filenames;
        this.numberOfThreads = numberOfThreads;
        this.images = Utils.loadImages(filenames);
        this.threads = new ArrayList<>();
    }

    // Highlight Fires.
    public void highLightFireFilter(String outputFile, float threshold) {
        long startTime = System.currentTimeMillis();

        //Logic from here: https://stackoverflow.com/a/51271170
        int numberOfRows = images.get(0).length;
        int length = numberOfRows / numberOfThreads;
        int remaining = numberOfRows % numberOfThreads;
        int left = 0;

        Color[][] copyImage = Utils.copyImage(images.get(0));
        threads = new ArrayList<>();

        for (int i = 0; i < numberOfThreads; i++) {
            int leftAux = left;
            int right = leftAux + (length - 1) + (remaining > 0 ? 1 : 0);

            HighLightFireFilterThread t = new HighLightFireFilterThread(copyImage, threshold, leftAux, right);
            threads.add(t);
            t.start();

            remaining--;
            left = right + 1;
        }

        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException ignored) {
            }
        });

        Utils.writeImage(copyImage, outputFile);

        long endTime = System.currentTimeMillis();
        logExecutionTimeOfMethod(startTime, endTime, HIGHLIGHT_FIRE_FILTER_MULTITHREADED.name());
        saveExecutionDetails(endTime - startTime,
                HIGHLIGHT_FIRE_FILTER_MULTITHREADED.name() + " " + Utils.dateFormat.format(startTime), numberOfThreads, images);
    }

    //Clean Image
    public void cleanImageFilter(String outputFile) {
        long startTime = System.currentTimeMillis();

        //Logic from here: https://stackoverflow.com/a/51271170
        int numberOfRows = images.get(0).length;
        int length = numberOfRows / numberOfThreads;
        int remaining = numberOfRows % numberOfThreads;
        int left = 0;

        Color[][] copyImage = Utils.copyImage(images.get(0));
        threads = new ArrayList<>();

        for (int i = 0; i < numberOfThreads; i++) {
            int leftAux = left;
            int right = leftAux + (length - 1) + (remaining > 0 ? 1 : 0);

            CleanImageFilterThread t = new CleanImageFilterThread(copyImage, images, leftAux, right);
            threads.add(t);
            t.start();

            remaining--;
            left = right + 1;
        }

        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException ignored) {
            }
        });

        Utils.writeImage(copyImage, outputFile);

        long endTime = System.currentTimeMillis();
        logExecutionTimeOfMethod(startTime, endTime, CLEAN_IMAGE_FILTER_MULTITHREADED.name());
        saveExecutionDetails(endTime - startTime,
                CLEAN_IMAGE_FILTER_MULTITHREADED.name() + " " + Utils.dateFormat.format(startTime), numberOfThreads, images);
    }
}

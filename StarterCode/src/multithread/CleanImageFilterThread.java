package multithread;

import java.awt.*;
import java.util.List;

public class CleanImageFilterThread extends Thread {

    public Color[][] image;
    public List<Color[][]> images;
    public int leftLimit;
    public int rightLimit;

    public CleanImageFilterThread(Color[][] image, List<Color[][]> images, int leftLimit, int rightLimit) {
        this.image = image;
        this.images = images;
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }

    public void run() {
        cleanImageFilterAux();
    }

    private void cleanImageFilterAux() {

        for (int i = leftLimit; i <= rightLimit; i++) {
            for (int j = 0; j < image[0].length; j++) {
                double sumRed = 0;
                double sumGreen = 0;
                double sumBlue = 0;

                for (Color[][] image : images) {
                    // fetches values of each pixel
                    Color pixel = image[i][j];
                    double r = pixel.getRed();
                    double g = pixel.getGreen();
                    double b = pixel.getBlue();

                    sumRed += r;
                    sumGreen += g;
                    sumBlue += b;
                }

                double avgRed = sumRed / images.size();
                double avgGreen = sumGreen / images.size();
                double avgBlue = sumBlue / images.size();

                double lowestDistance = Double.MAX_VALUE;

                for (Color[][] image : images) {
                    Color pixel = image[i][j];
                    double r = pixel.getRed();
                    double g = pixel.getGreen();
                    double b = pixel.getBlue();

                    double d = Math.sqrt(Math.pow((avgRed - r), 2) +
                            Math.pow((avgGreen - g), 2) +
                            Math.pow((avgBlue - b), 2));

                    if (d <= lowestDistance) {
                        lowestDistance = d;
                        image[i][j] = pixel;
                    }
                }
            }
        }
    }
}

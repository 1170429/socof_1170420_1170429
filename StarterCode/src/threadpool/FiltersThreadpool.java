package threadpool;

import utils.Utils;

import java.awt.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static utils.FiltersMethods.CLEAN_IMAGE_FILTER_THREADPOOL;
import static utils.FiltersMethods.HIGHLIGHT_FIRE_FILTER_THREADPOOL;
import static utils.Utils.logExecutionTimeOfMethod;
import static utils.Utils.saveExecutionDetails;

public class FiltersThreadpool {

    List<String> files;
    int numberOfThreads;
    List<Color[][]> images;

    // Constructor with filename for source image
    FiltersThreadpool(List<String> filenames, int numberOfThreads) {
        this.files = filenames;
        this.numberOfThreads = numberOfThreads;
        this.images = Utils.loadImages(filenames);
    }

    // Highlight Fires.
    public void highLightFireFilter(String outputFile, float threshold) throws InterruptedException {
        long startTime = System.currentTimeMillis();

        //Logic from here: https://stackoverflow.com/a/51271170
        int numberOfRows = images.get(0).length;
        int length = numberOfRows / numberOfThreads;
        int remaining = numberOfRows % numberOfThreads;
        int left = 0;

        Color[][] copyImage = Utils.copyImage(images.get(0));

        ExecutorService executor = new ThreadPoolExecutor(1, numberOfThreads,
                60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());

        for (int i = 0; i < numberOfThreads; i++) {
            int leftAux = left;
            int right = leftAux + (length - 1) + (remaining > 0 ? 1 : 0);

            HighLightFireFilterTask t = new HighLightFireFilterTask(copyImage, threshold, leftAux, right);
            executor.execute(t);

            remaining--;
            left = right + 1;
        }

        executor.shutdown();

        Utils.writeImage(copyImage, outputFile);

        long endTime = System.currentTimeMillis();
        logExecutionTimeOfMethod(startTime, endTime, HIGHLIGHT_FIRE_FILTER_THREADPOOL.name());
        saveExecutionDetails(endTime - startTime,
                HIGHLIGHT_FIRE_FILTER_THREADPOOL.name() + " " + Utils.dateFormat.format(startTime), numberOfThreads, images);
    }

    //Clean Image
    public void cleanImageFilter(String outputFile) {
        long startTime = System.currentTimeMillis();

        //Logic from here: https://stackoverflow.com/a/51271170
        int numberOfRows = images.get(0).length;
        int length = numberOfRows / numberOfThreads;
        int remaining = numberOfRows % numberOfThreads;
        int left = 0;

        Color[][] copyImage = Utils.copyImage(images.get(0));

        ExecutorService executor = new ThreadPoolExecutor(1, numberOfThreads,
                60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());

        for (int i = 0; i < numberOfThreads; i++) {
            int leftAux = left;
            int right = leftAux + (length - 1) + (remaining > 0 ? 1 : 0);

            CleanImageFilterTask t = new CleanImageFilterTask(copyImage, images, leftAux, right);
            executor.execute(t);

            remaining--;
            left = right + 1;
        }

        executor.shutdown();

        Utils.writeImage(copyImage, outputFile);

        long endTime = System.currentTimeMillis();
        logExecutionTimeOfMethod(startTime, endTime, CLEAN_IMAGE_FILTER_THREADPOOL.name());
        saveExecutionDetails(endTime - startTime,
                CLEAN_IMAGE_FILTER_THREADPOOL.name() + " " + Utils.dateFormat.format(startTime), numberOfThreads, images);
    }

}

package threadpool;

import java.awt.*;

public class HighLightFireFilterTask implements Runnable {

    public Color[][] image;
    public float threshold;
    public int leftLimit;
    public int rightLimit;

    public HighLightFireFilterTask(Color[][] image, float threshold, int leftLimit, int rightLimit) {
        this.image = image;
        this.threshold = threshold;
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }

    public void run() {
        highLightFireFilterAux();
    }

    private void highLightFireFilterAux() {

        for (int i = leftLimit; i <= rightLimit; i++) {
            for (int j = 0; j < image[i].length; j++) {
                // fetches values of each pixel
                Color pixel = image[i][j];
                int r = pixel.getRed();
                int g = pixel.getGreen();
                int b = pixel.getBlue();
                // takes average of color values
                int avg = (r + g + b) / 3;
                if (r > avg * threshold && g < 100 && b < 200)
                    // outputs red pixel
                    image[i][j] = new Color(255, 0, 0);
                else
                    image[i][j] = new Color(avg, avg, avg);

            }
        }
    }
}

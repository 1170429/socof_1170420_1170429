package threadpool;

import utils.FiltersMethods;
import utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static utils.Utils.printCharts;
import static utils.Utils.printExecutionTimeTable;

public class ApplyFiltersThreadPool {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Initializing...");

        String DEFAULT_OUT_HF = "out_highlightFiresThreadPool.jpg";
        String DEFAULT_OUT_CI = "out_cleanImageThreadPool.jpg";

        Scanner input = new Scanner(System.in);

        System.out.println(Utils.menuOptions);
        int nFilter = input.nextInt();
        input.nextLine();

        while (nFilter != 0) {
            String filePath;
            List<String> filePaths = new ArrayList<>();
            FiltersThreadpool filters = new FiltersThreadpool(filePaths, 1);

            if (nFilter < 3) {
                System.out.println("Insert the number of threads you would like to use:");
                int numberOfThreads = input.nextInt();
                input.nextLine();

                do {
                    System.out.println("Insert the path of the files you would like to use (0 to finish):");
                    filePath = input.nextLine();

                    if (!filePath.equals("0")) filePaths.add(filePath);

                } while (!filePath.equals("0") && nFilter != 1);

                System.out.println("Loading images...");
                filters = new FiltersThreadpool(filePaths, numberOfThreads);
            }

            switch (nFilter) {
                case 1://Highlight Fires
                    System.out.println("Insert the red value threshold:");
                    float inputtedThreshold = input.nextFloat();

                    System.out.println("Highlight Fires Filter");
                    filters.highLightFireFilter(DEFAULT_OUT_HF, inputtedThreshold);
                    break;
                case 2://Clean Image
                    System.out.println("Clean Image Filter");
                    filters.cleanImageFilter(DEFAULT_OUT_CI);
                    break;
                case 3://Print Charts
                    System.out.println("Printing Charts");
                    printCharts(FiltersMethods.THREADPOOL.name());
                case 4://Print Execution Time Table
                    System.out.println("Printing Execution Time Table");
                    printExecutionTimeTable();
                default:
                    break;
            }
            System.out.println();
            System.out.println("Successfully finished processing the images.");
            System.out.println();

            System.out.println(Utils.menuOptions);
            nFilter = input.nextInt();
            input.nextLine();
        }
        System.out.println("Terminating...");
        input.close();
    }

}

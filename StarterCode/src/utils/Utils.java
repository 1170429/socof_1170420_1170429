package utils;

import utils.chats.ChartEntry;
import utils.chats.LineChart;
import utils.tables.ExecutionTimeTable;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    public static List<ChartEntry> chartEntries = new ArrayList<>();
    public static DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
    public static String menuOptions = "Choose which filter you would like to use:\n" +
            "1 - HighlightFires\n" +
            "2 - CleanImage\n" +
            "3 - Print Charts\n" +
            "4 - Print Execution Time Table\n" +
            "0 - Exit";

    Utils() {
    }

    /**
     * Loads image from filename into a Color (pixels decribed with rgb values) matrix.
     *
     * @param filenames the name of the imge in the filesystem.
     * @return Color matrix.
     */
    public static List<Color[][]> loadImages(List<String> filenames) {
        List<Color[][]> imgs = new ArrayList<>();

        filenames.forEach(filename -> {
            BufferedImage buffImg = loadImageFile(filename);
            Color[][] colorImg = convertTo2DFromBuffered(buffImg);
            imgs.add(colorImg);
        });

        return imgs;
    }

    /**
     * Converts image from a Color matrix to a .jpg file.
     *
     * @param image    the matrix of Color objects.
     * @param filename to the image.
     */
    public static void writeImage(Color[][] image, String filename) {
        File outputfile = new File(filename);
        var bufferedImage = Utils.matrixToBuffered(image);
        try {
            ImageIO.write(bufferedImage, "jpg", outputfile);
        } catch (IOException e) {
            System.out.println("Could not write image " + filename + " !");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Copy a Color matrix to another Color matrix.
     * Useful if one does not want to modify the original image.
     *
     * @param image the source matrix
     * @return a copy of the image
     */

    public static Color[][] copyImage(Color[][] image) {
        Color[][] copy = new Color[image.length][image[0].length];
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                copy[i][j] = image[i][j];
            }
        }
        return copy;
    }

    public static void logExecutionTimeOfMethod(long startTime, long endTime, String methodName) {
        long duration = (endTime - startTime);

        System.out.println("----------------------- Execution Time --------------------------");
        System.out.println("The method " + methodName + " took " + duration + " milliseconds.");
        System.out.println("-----------------------------------------------------------------");
    }

    public static void saveExecutionDetails(long executionTime, String methodName, int numberOfThreads, List<Color[][]> images) {
        List<Integer> numberOfPixelsPerImage = images.stream().map(image -> image.length * image[0].length)
                .collect(Collectors.toList());
        chartEntries.add(new ChartEntry(executionTime, methodName, numberOfThreads, images.size(), numberOfPixelsPerImage));
    }

    public static void printCharts(String name) throws IOException {
        new LineChart(name);
    }

    public static void printExecutionTimeTable() {
        new ExecutionTimeTable().print();
    }

    /**
     * Loads in a BufferedImage from the specified path to be processed.
     *
     * @param filename The path to the file to read.
     * @return a BufferedImage if able to be read, NULL otherwise.
     */
    private static BufferedImage loadImageFile(String filename) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(filename));
        } catch (IOException e) {
            System.out.println("Could not load image " + filename + " !");
            e.printStackTrace();
            System.exit(1);
        }
        return img;
    }

    /**
     * Converts a matrix of Colors into a BufferedImage to
     * write on the filesystem.
     *
     * @param image the matrix of Colors
     * @return the image ready for writing to filesystem
     */
    private static BufferedImage matrixToBuffered(Color[][] image) {
        int width = image.length;
        int height = image[0].length;
        BufferedImage bImg = new BufferedImage(width, height, 1);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bImg.setRGB(x, y, image[x][y].getRGB());
            }
        }
        return bImg;
    }

    /**
     * Converts a file loaded into a BufferedImage to a
     * matrix of Colors
     *
     * @param image the BufferedImage to convert
     * @return the matrix of Colors
     */

    private static Color[][] convertTo2DFromBuffered(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        Color[][] result = new Color[width][height];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                // Get the integer RGB, and separate it into individual components.
                // (BufferedImage saves RGB as a single integer value).
                int pixel = image.getRGB(x, y);
                //int alpha = (pixel >> 24) & 0xFF;
                int red = (pixel >> 16) & 0xFF;
                int green = (pixel >> 8) & 0xFF;
                int blue = pixel & 0xFF;
                result[x][y] = new Color(red, green, blue);
            }
        }
        return result;
    }

}
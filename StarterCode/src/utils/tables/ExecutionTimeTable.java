package utils.tables;

import net.steppschuh.markdowngenerator.table.Table;
import utils.Utils;
import utils.chats.ChartEntry;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ExecutionTimeTable {

    private final List<ChartEntry> chartEntries;
    private Table.Builder tableBuilder;

    public ExecutionTimeTable() {
        this.chartEntries = Utils.chartEntries;
        init();
    }

    private void init() {
        this.tableBuilder = new Table.Builder()
                .withAlignments(Table.ALIGN_CENTER, Table.ALIGN_CENTER, Table.ALIGN_CENTER, Table.ALIGN_CENTER, Table.ALIGN_CENTER, Table.ALIGN_CENTER, Table.ALIGN_CENTER)
                .addRow("Filter Name", "Number of Threads", "Number of Images", "Number of Pixels per Image", "Execution Time");

        //sortByName
        Collections.sort(chartEntries);

        chartEntries.forEach(chartEntry -> {
            String pixelsPerImage = chartEntry.numberOfPixelsPerImage.stream().map(String::valueOf).collect(Collectors.joining(", "));
            tableBuilder.addRow(chartEntry.filterName, String.valueOf(chartEntry.numberOfThreads),
                    String.valueOf(chartEntry.numberOfImages), pixelsPerImage, String.valueOf(chartEntry.executionTime));
        });
    }

    public void print() {
        System.out.println(tableBuilder.build());
    }
}

package utils.chats;

import java.util.List;

public class ChartEntry implements Comparable<ChartEntry> {

    public long executionTime;
    public String filterName;
    public int numberOfThreads;
    public int numberOfImages;
    public List<Integer> numberOfPixelsPerImage;

    public ChartEntry(long executionTime, String filterName, int numberOfThreads, int numberOfImages, List<Integer> numberOfPixelsPerImage) {
        this.executionTime = executionTime;
        this.filterName = filterName;
        this.numberOfThreads = numberOfThreads;
        this.numberOfImages = numberOfImages;
        this.numberOfPixelsPerImage = numberOfPixelsPerImage;
    }

    @Override
    public int compareTo(ChartEntry o) {
        return this.filterName.compareTo(o.filterName);
    }
}

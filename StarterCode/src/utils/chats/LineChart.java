package utils.chats;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class LineChart extends JFrame {

    private final List<ChartEntry> chartEntries;
    private final String name;

    public LineChart(String name) throws HeadlessException, IOException {
        this.chartEntries = Utils.chartEntries;
        this.name = name;
        init();
    }

    private void init() throws IOException {
        XYDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        chartPanel.setBackground(Color.white);

        add(chartPanel);

        ChartUtils.saveChartAsPNG(new File(name.toLowerCase() + "_execution_times.png"), chart, 550, 490);
    }

    private XYDataset createDataset() {
        XYSeriesCollection dataset = new XYSeriesCollection();

        chartEntries.forEach(chartEntry -> {
            XYSeries series = new XYSeries(chartEntry.filterName);
            series.add(chartEntry.executionTime, chartEntry.numberOfThreads);

            dataset.addSeries(series);
        });

        return dataset;
    }

    private JFreeChart createChart(final XYDataset dataset) {
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Execution Times",
                "Time",
                "Number of Threads",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        XYPlot plot = chart.getXYPlot();

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

        for (int i = 0; i < chartEntries.size(); i++) {
            renderer.setSeriesPaint(i, getRandomColor());
            renderer.setSeriesStroke(i, new BasicStroke(2.0f));
        }

        plot.setRenderer(renderer);
        plot.setBackgroundPaint(Color.white);
        plot.setRangeGridlinesVisible(false);
        plot.setDomainGridlinesVisible(false);

        chart.getLegend().setFrame(BlockBorder.NONE);

        chart.setTitle(new TextTitle("Execution Times",
                new Font("Serif", Font.BOLD, 18))
        );

        return chart;
    }

    private Color getRandomColor() {
        int R = (int) Math.round(Math.random() * 255);
        int G = (int) Math.round(Math.random() * 255);
        int B = (int) Math.round(Math.random() * 255);

        return new Color(R, G, B);
    }

}

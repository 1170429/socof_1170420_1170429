package utils;

public interface Filters {

    void highLightFireFilter(String outputFile, float threshold);

    void cleanImageFilter(String outputFile);
}

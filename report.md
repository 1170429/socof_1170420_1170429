# Software Concorrente e Fiável

### Mestrado em Engenharia Informática
### Instituto Superior de Engenharia do Porto

---
# Digital Image Processing and Analysis in Multicore Systems

---

**Turma:** M1OP8


**Autores:**
1. 1170420 - João Nascimento
2. 1170429 - Fábio Alves

---

## 1. Introdução

A unidade curricular de SOCOF (Sistemas Concorrentes e Fiáveis) pretende a elaboração de um projeto onde se requer
o desenvolvimento de uma aplicação de modo a se aplicar e consolidar as temáticas lecionadas durante o semestre.

Para tal, foi apresentado um enunciado relativo ao processamento digital de imagem e à aplicação de dois filtros distintos sobre imagens: 
a) o primeiro, denomidado de *Highlight Forest Fires*, onde se pretende, através de um valor limite, identificar zonas de fogo (a vermelho), 
de uma imagem e salientar essas zonas transformando os pixels para vermelho, e as restantes zonas para tons preto e branco; 
b) o segundo, com o nome *Clean Image*, tendo o objetivo de remover objetos e pessoas de imagens de modo a criar um apenas com os pixels comuns a todas as imagens.

Deste modo, o presente trabalho tem como principal objetivo a implementação de uma aplicação de edição de imagem que utilize diferentes abordagens de software concorrente de forma a que se realize o respetivo estudo e análise da performance segundo diferentes abordagens, sequencial ou multithreaded, de aplicação de filtros a imagens. 
Como diferentes técnicas de processamento digital de imagem concorrente prevê-se o uso de uma abordagem Sequencial, Multithreaded e Threadpool.

Inicialmente, começa-se por apresentar os 3 modos de implementação diferentes, onde se refere a lógica da implementação em cada abordagem.
É descrita a implementação dos filtros no modo sequencial bem como as adaptações feitas ao algoritmo para as abordagens *multithreaded* e com base em *threadpool*.

De seguida, apresentam-se os resultados de cada implementação e estes são apresentados através de tabelas geradas automaticamente pela aplicação com os tempos de execução de cada algoritmo nas diferentes abordagens, variando os diferentes fatores de *input*, como número de imagens, tamanho das mesmas, número de *threads* e, no caso da *threadpool*, diferentes configurações.

Por fim, é feita a discussão dos resultados apresentados de forma a realçar as vantagens e desvantagens de cada abordagem, bem como, apresentar as melhores configurações utilizadas na abordagem com base em *threadpool*, no contexto do problema.

## 2. Implementação dos Filtros

Neste capítulo faz-se uma descrição detalhada sobre o modo de estruturação do código da aplicação e dos algoritmos pensadados para cada um dos filtros.

Começa-se por identificar e explicar os blocos de código comuns entre as abordagens de implementação de cada filtro, onde também se explica como se construiu o gráfico e tabela relativa aos detalhes do processamento efetuado.

Posteriormente, explicam-se os detalhes diferenciadores relativos a cada abordagens de implementação dos filtros: Sequencial, *Multithreaded* e *Threadpool*.  

## 2.1 Implementação comuns a todas as abordagens

O projeto divide-se em 4 grandes partes, uma para a abordagem Sequencial, para a Multithreaded, para a Threadpool e outra para classes de Utilidades (*Utils*).

Assim sendo, cada abordagem tem o seu próprio *main* é composto por um ciclo *while* onde se insere um menu. Este menu permite ao utilizador escolher
o que prentede realizar na aplicação, estando as opções definidas numa variavel *static* na classe *Utils*:

```
public static String menuOptions = 
        "Choose which filter you would like to use:\n" +
            "1 - HighlightFires\n" +
            "2 - CleanImage\n" +
            "3 - Print Charts\n" +
            "4 - Print Execution Time Table\n" +
            "0 - Exit";
```

De seguida, e já no ciclo *while* questiona-se o utilizador do caminho para a imagem que se pretende processar. 
Se for para processar mais que uma imagem, o utilizador só terá de inserir de novo um caminho, uma vez que, para terminar
de pedir caminhos, o utilizador terá de introduzir o número 0:

```
while (nFilter != 0) {
   (...)
   
   do {
        System.out.println("Insert the path of the files you would like to use (0 to finish):");
        filePath = input.nextLine();
    
        if (!filePath.equals("0")) filePaths.add(filePath);

   } while (!filePath.equals("0") && nFilter != 1);

   System.out.println("Loading images...");
   filters = new FiltersSequential(filePaths);
   
   (...)
}
```

Posteriormente, processa-se o pedido de processamento que o utilizador pretendo através de um *switch case* onde se irá chamar o método
de aplicação de filtro pretendido ou se irá invocar um método *static* para mostrar gráficos ou uma tabela com a informação dos tempos de processamento:

```
while (nFilter != 0) {
    (...)
    
    switch (nFilter) {
        case 1://Highlight Fires
            System.out.println("Insert the red value threshold:");
            float inputtedThreshold = input.nextFloat();
    
            System.out.println("Highlight Fires Filter");
            filters.highLightFireFilter(DEFAULT_OUT_HF, inputtedThreshold);
            break;
        case 2://Clean Image
            System.out.println("Clean Image Filter");
            filters.cleanImageFilter(DEFAULT_OUT_CI);
            break;
        case 3://Print Charts
            System.out.println("Printing Charts");
            Utils.printCharts(FiltersMethods.SEQUENTIAL.name());
        case 4://Print Execution Time Table
            System.out.println("Printing Execution Time Table");
            Utils.printExecutionTimeTable();
        default:
            break;
    }
    
    (...)
}
```

Por fim, volta-se a permitir ao utilizador a possibilidade de aplicação de um novo filtro a uma nova imagem: 

```
while (nFilter != 0) {
    (...)
    
    System.out.println(Utils.menuOptions);
    nFilter = input.nextInt();
    input.nextLine();
    
    (...)
}
```

No que diz respeito ao tratamento das diferentes abordagens de aplicação de filtros, existem 3 classes distintas para cada uma:

1. FiltersSequential
2. FiltersMultithreaded
3. FiltersThreadpool

Cada uma destas classes implementa a interface *Filters* de modo a implementar cada filtro de acordo com cada abordagem de concorrência
pretendia:

```
public interface Filters {

    void highLightFireFilter(String outputFile, float threshold);

    void cleanImageFilter(String outputFile);
}
```

A base dos algoritmos para a aplicação dos filtros são comuns a todas as abordagens. 
Assim sendo, relativamente ao filtro *HighlightFires*, percorre-se a matriz para se aceder a todos os pixels que compoem a imagem.
A estes pixeis, obtem-se o valor do *Red*, *Green* e *Blue* para se poder calcular, em função do valor threshold para vermelho, se é para marcar o pixel com vermelho ou se é para se passar para tons de cinza:

```
for (int i = 0; i < copyImage.length; i++) {
    for (int j = 0; j < copyImage[0].length; j++) {
        // fetches values of each pixel
        Color pixel = copyImage[i][j];
        int r = pixel.getRed();
        int g = pixel.getGreen();
        int b = pixel.getBlue();
        // takes average of color values
        int avg = (r + g + b) / 3;
        if (r > avg * threshold && g < 100 && b < 200)
            // outputs red pixel
            copyImage[i][j] = new Color(255, 0, 0);
        else
            copyImage[i][j] = new Color(avg, avg, avg);

    }
}
```

Já no filtro para limpar a imagem, começa-se por percorrer a matriz que representa a imagem e, para cada posição dos pixeis, somam-se os valores de *Red*, *Green* e *Blue*
de todas as imagens selecionadas para processamento. Deste modo, consegue-se cacular a média para cada cor, a fim de se conseguir calcular a distância do pixel relativamente à média e saber se 
é para substituir ou não o pixel:

```
for (int i = 0; i < copyImage.length; i++) {
    for (int j = 0; j < copyImage[0].length; j++) {
        double sumRed = 0;
        double sumGreen = 0;
        double sumBlue = 0;

        for (Color[][] image : images) {
            // fetches values of each pixel
            Color pixel = image[i][j];
            double r = pixel.getRed();
            double g = pixel.getGreen();
            double b = pixel.getBlue();

            sumRed += r;
            sumGreen += g;
            sumBlue += b;
        }

        double avgRed = sumRed / images.size();
        double avgGreen = sumGreen / images.size();
        double avgBlue = sumBlue / images.size();

        double lowestDistance = Double.MAX_VALUE;

        for (Color[][] image : images) {
            Color pixel = image[i][j];
            double r = pixel.getRed();
            double g = pixel.getGreen();
            double b = pixel.getBlue();

            double d = Math.sqrt(Math.pow((avgRed - r), 2) +
                    Math.pow((avgGreen - g), 2) +
                    Math.pow((avgBlue - b), 2));

            if (d <= lowestDistance) {
                lowestDistance = d;
                copyImage[i][j] = pixel;
            }
        }
    }
}
```


No final de ambos os filtros e para qualquer abordagem, faz-se uso da classe auxiliar *Utils* para converter a matriz processada numa nova imagem e mostrar na consola os tempos de execução. Exemplo:

```
Utils.writeImage(copyImage, outputFile);

long endTime = System.currentTimeMillis();
Utils.logExecutionTimeOfMethod(startTime, endTime, HIGHLIGHT_FIRE_FILTER_SEQUENTIAL.name());
Utils.saveExecutionDetails(endTime - startTime,
    HIGHLIGHT_FIRE_FILTER_SEQUENTIAL.name() + " " + Utils.dateFormat.format(startTime), 1, images);
```

É possível verificar que ainda se invoca um método *saveExecutionDetails* que irá permitir guardar os detalhes desta execução numa lista de objetos do tipo *ChartEntry*. 
Este objeto contém:

1. Tempo de execução
2. Nome do filtro aplicado
3. Número de threads usadas
4. Número de imagens introduzidas inicialmente para processamento
5. Tamanho das imagens processadas

Tendo estes dados guardados, quando o utilizador escolhe a opção do menu para produzir um gráfico com os tempos de execução, 
invoca-se um método da classe *Utils* que por sua cria um objeto do tipo *LineChart*.
Está classe, permite criar um gráfico usando a biblioteca *jfreechart* adicionada como dependência ao pom do projeto maven.

Exemplo do gráfico produzido:
![example_chart_execution_times.png](example_chart_execution_times.png)

Relativamente à opção do menu que permite criar uma tabela com todos os detallhes do processamento, também se recorreu a uma 
biblioteca externa, *markdowngenerator*, para construir uma tabela MarkDown com base na lista de objetos do tipo *ChartEntry* previamente guardado.

Exemplo de uma tabela produzida:

```
Printing Execution Time Table
|                      Filter Name                      | Number of Threads | Number of Images |  Number of Pixels per Image  | Execution Time |
|:-----------------------------------------------------:|:-----------------:|:----------------:|:----------------------------:|:--------------:|
|  CLEAN_IMAGE_FILTER_SEQUENTIAL 02 Jun 2022 22:08:43   |         1         |        2         |      12000000, 12000000      |      2232      |
|  CLEAN_IMAGE_FILTER_SEQUENTIAL 02 Jun 2022 22:09:03   |         1         |        3         | 12000000, 12000000, 12000000 |      2743      |
| HIGHLIGHT_FIRE_FILTER_SEQUENTIAL 02 Jun 2022 22:08:07 |         1         |        1         |           20353344           |      3646      |
| HIGHLIGHT_FIRE_FILTER_SEQUENTIAL 02 Jun 2022 22:08:23 |         1         |        1         |           3377152            |      387       |
```

## 2.1 Implementação Sequencial

A implementação sequencial segue ao detalhe o algoritmo apresentado anteriormente, uma vez que, não se teve de dividir a imagem em várias partes para processamento paralelo.

No que diz respeito ao filtro *HighlightFires*, recebe-se por parametro o nome que se quer atribuir à imagem produzida (por default será *out_highlightFires[TipoDeFiltro]*) e o valor do threshold a usar nos cálculos.
Relativamente ao algoritmo, obtem-se o tempo atual do sistema para mais tarde se poder calcular o tempo de execução e copia-se a imagem recebida inicialmente para cálculos posteriores à escrita:

```
public void highLightFireFilter(String outputFile, float threshold) {
    long startTime = System.currentTimeMillis();

    Color[][] copyImage = Utils.copyImage(images.get(0));
    (...)
}
```

De seguida vêm os ciclos *for* para o processamento por pixel, explicado no capítulo anterior e gera-se a nova imagem produzida. Para além disso,
mostra-se o tempo de execução na consola e guardam-se os detalhes do processamento para mais tarde ser possível contruír o gráfico e/ou a tabela:

```
(...)
 Utils.writeImage(copyImage, outputFile);

long endTime = System.currentTimeMillis();
logExecutionTimeOfMethod(startTime, endTime, HIGHLIGHT_FIRE_FILTER_SEQUENTIAL.name());
saveExecutionDetails(endTime - startTime,
        HIGHLIGHT_FIRE_FILTER_SEQUENTIAL.name() + " " + Utils.dateFormat.format(startTime), 1, images);
```

Do mesmo modo, também no *cleanImageFilter* se começa por receber o nome que se irá atribuir à imagem produzida (*out_CleanImage[TipoDeFiltro]* por default) por parametro e
obtem-se o tempo atual do sistema para se calcular o tempo de execução seguindo-se a cópia da imagem recebida.

Posteriormente passa-se para o algoritmo de processamento do filtro e, no fim, gera-se a nova imagem, mostra-se o tempo de execução na consola.
e guarda-se os dados da mesma:

```
(...)
Utils.writeImage(copyImage, outputFile);

long endTime = System.currentTimeMillis();
logExecutionTimeOfMethod(startTime, endTime, CLEAN_IMAGE_FILTER_SEQUENTIAL.name());
saveExecutionDetails(endTime - startTime,
        CLEAN_IMAGE_FILTER_SEQUENTIAL.name() + " " + Utils.dateFormat.format(startTime), 1, images);
```

## 2.2 Implementação Multithreaded

A abordagem *multithreaded* tem como objetivo otimizar a aplicação dos filtros, dividindo as imagens e distribuindo o trabalho por diferentes *threads*. O problema em questão enquadra-se no conceito de paralelismo e, portanto, não foi utilizado nenhum controlo de concorrência.

Desta forma, optou-se por obter o número de *threads* a utilizar através de input do utilizador. De seguida, o número total de linhas da matriz da imagem é dividida pelo número de *threads* introduzido, de forma a obter à priori a carga de trabalho a ser realizado por cada *thread*.

Assim, foram criadas duas classes *HighLightFireFilterThread* e *CleanImageFilterThread* que extendem a classe *Thread* e implementam o método *run*(). A classe *HighLightFireFilterThread* recebe no construtor a matriz na qual vai ser escrito o resultado do processamento, o *threshold* utilizado no filtro e os limites do intervalo da matriz que a thread deve processar.

```
public class HighLightFireFilterThread extends Thread {
    public Color[][] image;
    public float threshold;
    public int leftLimit;
    public int rightLimit;

    public HighLightFireFilterThread(Color[][] image, float threshold, int leftLimit, int rightLimit) {
        this.image = image;
        this.threshold = threshold;
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }
    
    public void run() {
        highLightFireFilterAux();
    }
    (...)
```

Já o construtor da classe *CleanImageFilterThread* recebe a matriz na qual são escritos os resultados do processamento, a lista das imagens de *input* utilizadas e também os limites do intervalo da matriz a processar.

```
public class CleanImageFilterThread extends Thread {

    public Color[][] image;
    public List<Color[][]> images;
    public int leftLimit;
    public int rightLimit;

    public CleanImageFilterThread(Color[][] image, List<Color[][]> images, int leftLimit, int rightLimit) {
        this.image = image;
        this.images = images;
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }
    
    public void run() {
        cleanImageFilterAux();
    }
    (...)
```

A lógica para o processamento *multithreaded* inicia-se, então, pela divisão do total de linhas da matriz de *input* pelo número de *threads* introduzido e por obter os limites da matriz a processar. De seguida, num ciclo *for* são instanciadas as *threads* com os limites calculados e estas são adicionadas a um *ArrayList*. Por fim, espera-se pelo fim do processamento de todas as *threads* instanciadas, utilizando o método *join*(), e é escrita a matriz resultante para o ficheiro de *output*.

```
public void cleanImageFilter(String outputFile) {
    (...)

    //Logic from here: https://stackoverflow.com/a/51271170
    int numberOfRows = images.get(0).length;
    int length = numberOfRows / numberOfThreads;
    int remaining = numberOfRows % numberOfThreads;
    int left = 0;

    Color[][] copyImage = Utils.copyImage(images.get(0));
    threads = new ArrayList<>();

    for (int i = 0; i < numberOfThreads; i++) {
        int leftAux = left;
        int right = leftAux + (length - 1) + (remaining > 0 ? 1 : 0);

        CleanImageFilterThread t = new CleanImageFilterThread(copyImage, images, leftAux, right);
        threads.add(t);
        t.start();

        remaining--;
        left = right + 1;
    }

    threads.forEach(t -> {
        try {
            t.join();
        } catch (InterruptedException ignored) {
        }
    });

    Utils.writeImage(copyImage, outputFile);

    (...)
}
```

## 2.3 Implementação baseada em Threadpool

A implementação baseada em *threadpool* tem como objetivo atribuir a responsabilidade de criar e gerir as threads utilizadas no processamento das imagens a uma *threadpool*.

Manteve-se a mesma abordagem em relação à divisão da imagem em partes, sendo o número de partes escolhido pelo utilizador. À semelhança do ponto anterior, criaram-se duas novas classes *HighLightFireFilterTask* e *CleanImageFilterTask* que representam as tarefas a ser executadas pelo *ExecutorService*. Desta vez, as classes implementam a interface *Runnable* e o seu método *run*().

```
public class CleanImageFilterTask implements Runnable {

    public Color[][] image;
    public List<Color[][]> images;
    public int leftLimit;
    public int rightLimit;

    public CleanImageFilterTask(Color[][] image, List<Color[][]> images, int leftLimit, int rightLimit) {
        this.image = image;
        this.images = images;
        this.leftLimit = leftLimit;
        this.rightLimit = rightLimit;
    }
    
    public void run() {
        cleanImageFilterAux();
    }
    (...)
```

Foram exploradas diversas configurações na utilizações do *ExecutorService* e *threadpools*, nomeadamente, diferentes números de *threads* principais (*corePoolSize*), de *threads* secundárias (*maximumPoolSize*) e tempo de existência das *threads* secundárias (*keepAliveTime*). Utilizou-se a classe *ThreadPoolExecutor* de forma a configurar estes parâmetros manualmente e poder testar a performance com diferentes conjuntos de configurações.

```
public void highLightFireFilter(String outputFile, float threshold) throws InterruptedException {
    (...)

    ExecutorService executor = new ThreadPoolExecutor(1, numberOfThreads,
            60L, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>());

    for (int i = 0; i < numberOfThreads; i++) {
        (...)

        HighLightFireFilterTask t = new HighLightFireFilterTask(copyImage, threshold, leftAux, right);
        executor.execute(t);

        (...)
    }

    executor.shutdown();

    Utils.writeImage(copyImage, outputFile);

    (...)
}
```

Assim, à medida que são criadas tarefas, estas são enviadas para o *ExecutorService* que utiliza a *threadpool* para criar e gerir as *threads* que as executam. No final, utilizou-se o método *shutdown*() que espera que todas as tarefas na *queue* do *ExecuterService* terminem a sua execução e é criado o ficheiro de *output* da imagem processada.

## 3. Comparação da performance e discussão

Este capítulo tem com objetivo apresentar o estudo realizado relativamente às diferentes abordagens implementadas para cada filtro. 

Assim sendo, começa-se por mostrar as tabelas, geradas automaticamente pela aplicação, onde se pode observar os detalhes das diferentes execuções e as diferentes configurações usadas. De salientar que para cada configuração/imagem, correu-se o algoritmo 3 vezes de modo a se conseguir obter uma melhor média do tempo de execução.

De seguida, apresentam-se as conclusões retiradas com base nos resultados obtidos e onde se consegue consluir quais as melhores configurações para cada filtro.

### 3.1. Comparação da performance

#### 3.1.1 Sequencial

Na abordagem sequencial executaram-se ambos os filtros, sendo que para o filtro que permite identificar as zonas vermelhas,
correu-se o programa para as 3 imagens exemplo. Cada imagem tem tamanhos diferentes pelo que os tempos de execução diferem bastante por imagem, sendo tanto maiores quanto o número de pixéis da imagem:

| Number of Threads | Number of Images | Number of Pixels per Image | Execution Time |
| :---------------: | :--------------: | :------------------------: | :------------: |
|         1         |        1         |          20353344          |      2097      |
|         1         |        1         |          20353344          |      2223      |
|         1         |        1         |          20353344          |      2116      |
|         1         |        1         |          3377152           |      335       |
|         1         |        1         |          3377152           |      242       |
|         1         |        1         |          3377152           |      247       |
|         1         |        1         |           515200           |       38       |
|         1         |        1         |           515200           |       40       |
|         1         |        1         |           515200           |       39       |


No que diz respeito ao filtro para limpar as imagens, executou-se o processamento com 2, 3 e 4 imagens, uma vez que todas têm o mesmo número de pixéis, sendo que o tempo de execução aumentou gradualmente consoante o aumento do número de imagens utilizadas:

| Number of Threads | Number of Images |       Number of Pixels per Image       | Execution Time |
| :---------------: | :--------------: | :------------------------------------: | :------------: |
|         1         |        2         |           12000000, 12000000           |      1604      |
|         1         |        2         |           12000000, 12000000           |      1379      |
|         1         |        2         |           12000000, 12000000           |      1449      |
|         1         |        3         |      12000000, 12000000, 12000000      |      1905      |
|         1         |        3         |      12000000, 12000000, 12000000      |      1982      |
|         1         |        3         |      12000000, 12000000, 12000000      |      2363      |
|         1         |        4         | 12000000, 12000000, 12000000, 12000000 |      3877      |
|         1         |        4         | 12000000, 12000000, 12000000, 12000000 |      4036      |
|         1         |        4         | 12000000, 12000000, 12000000, 12000000 |      2702      |

#### 3.1.2 Multithreaded

Para a abordagem *Multithreaded*, decidiu-se testar o algoritmo produzido para o filtro *HighlightFires* para cada uma das 3 imagens fornecidas e com 5, 50 e 500 threads*.

Assim sendo, é possível comprovar que quanto maior o número de pixéis da imagem, maior o tempo de execução. Para além disso, nota-se, de um modo geral, um maior tempo de execução à medida que se aumenta o número de *threads* utilizadas no processamento. Os resultados obtidos foram os seguintes:

**Teste com 5 Threads:**

| Number of Threads | Number of Images | Number of Pixels per Image | Execution Time |
| :---------------: | :--------------: | :------------------------: | :------------: |
|         5         |        1         |          20353344          |      1836      |
|         5         |        1         |          20353344          |      1964      |
|         5         |        1         |          20353344          |      1932      |
|         5         |        1         |          3377152           |      381       |
|         5         |        1         |          3377152           |      355       |
|         5         |        1         |          3377152           |      367       |
|         5         |        1         |           515200           |       34       |
|         5         |        1         |           515200           |       34       |
|         5         |        1         |           515200           |       33       |

**Teste com 50 Threads:**

| Number of Threads | Number of Images | Number of Pixels per Image | Execution Time |
| :---------------: | :--------------: | :------------------------: | :------------: |
|        50         |        1         |          20353344          |      1940      |
|        50         |        1         |          20353344          |      1978      |
|        50         |        1         |          20353344          |      1970      |
|        50         |        1         |          3377152           |      353       |
|        50         |        1         |          3377152           |      357       |
|        50         |        1         |          3377152           |      371       |
|        50         |        1         |           515200           |       56       |
|        50         |        1         |           515200           |       75       |
|        50         |        1         |           515200           |       72       |

**Teste com 500 Threads:**

| Number of Threads | Number of Images | Number of Pixels per Image | Execution Time |
| :---------------: | :--------------: | :------------------------: | :------------: |
|        500        |        1         |          20353344          |      2055      |
|        500        |        1         |          20353344          |      2073      |
|        500        |        1         |          20353344          |      2123      |
|        500        |        1         |          3377152           |      444       |
|        500        |        1         |          3377152           |      388       |
|        500        |        1         |          3377152           |      397       |
|        500        |        1         |           515200           |      128       |
|        500        |        1         |           515200           |      121       |
|        500        |        1         |           515200           |      117       |

Quanto aos resultados do filtro *CleanImage*, executou-se o processamento com 2 e 3 imagens, sendo que o tempo de execução aumentou linearmente com o número de *threads* e/ou imagens utilizadas:

| Number of Threads | Number of Images | Number of Pixels per Image | Execution Time |
| :---------------: | :--------------: | :------------------------: | :------------: |
|         5         |        2         |     12000000, 12000000     |      1061      |
|         5         |        2         |     12000000, 12000000     |      993       |
|         5         |        2         |     12000000, 12000000     |      1096      |
|        50         |        2         |     12000000, 12000000     |      1112      |
|        50         |        2         |     12000000, 12000000     |      1204      |
|        50         |        2         |     12000000, 12000000     |      1093      |
|        500        |        2         |     12000000, 12000000     |      1064      |
|        500        |        2         |     12000000, 12000000     |      1076      |
|        500        |        2         |     12000000, 12000000     |      1121      |


| Number of Threads | Number of Images |  Number of Pixels per Image  | Execution Time |
| :---------------: | :--------------: | :--------------------------: | :------------: |
|         5         |        3         | 12000000, 12000000, 12000000 |      1258      |
|         5         |        3         | 12000000, 12000000, 12000000 |      1106      |
|         5         |        3         | 12000000, 12000000, 12000000 |      1174      |
|        50         |        3         | 12000000, 12000000, 12000000 |      1100      |
|        50         |        3         | 12000000, 12000000, 12000000 |      1225      |
|        50         |        3         | 12000000, 12000000, 12000000 |      1105      |
|        500        |        3         | 12000000, 12000000, 12000000 |      1162      |
|        500        |        3         | 12000000, 12000000, 12000000 |      1254      |
|        500        |        3         | 12000000, 12000000, 12000000 |      1183      |
#### 3.1.3 Threadpool

Para está abordagem foram idealizadas 3 *suites* de testes tendo em conta as seguintes possibilidades para valores do construtor da classe *ThreadPoolExecutor*:

1. **corePoolSize** = **maximumPoolSize** = número de partes em que se divide a imagem requeridas pelo utilizador; **keepAliveTime** = 0 segundos
2. **corePoolSize** = 1; **maximumPoolSize** = número de partes em que se divide a imagem requeridas pelo utilizador; **keepAliveTime** = 60 segundos
3. **corePoolSize** = 5; **maximumPoolSize** = número de partes em que se divide a imagem requeridas pelo utilizador; **keepAliveTime** = 0 segundos

Para cada *Suite*, testou-se com 5, 50, e 500 número de partes em que se divide a imagem.

Assim sendo, para o filtro dos fogos os resultados obtidos foram os seguintes:

**Suite 1 - 5 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|        5        |        1         |          20353344          |      2092      |
|        5        |        1         |          20353344          |      1970      |
|        5        |        1         |          20353344          |      1900      |
|        5        |        1         |          3377152           |      376       |
|        5        |        1         |          3377152           |      344       |
|        5        |        1         |          3377152           |      339       |
|        5        |        1         |           515200           |       72       |
|        5        |        1         |           515200           |       69       |
|        5        |        1         |           515200           |       71       |

**Suite 1 - 50 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|       50        |        1         |          20353344          |      2054      |
|       50        |        1         |          20353344          |      2101      |
|       50        |        1         |          20353344          |      1997      |
|       50        |        1         |          3377152           |      339       |
|       50        |        1         |          3377152           |      317       |
|       50        |        1         |          3377152           |      351       |
|       50        |        1         |           515200           |       79       |
|       50        |        1         |           515200           |       73       |
|       50        |        1         |           515200           |       72       |

**Suite 1 - 500 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|       500       |        1         |          20353344          |      2016      |
|       500       |        1         |          20353344          |      1805      |
|       500       |        1         |          20353344          |      1854      |
|       500       |        1         |          3377152           |      374       |
|       500       |        1         |          3377152           |      365       |
|       500       |        1         |          3377152           |      360       |
|       500       |        1         |           515200           |      127       |
|       500       |        1         |           515200           |      100       |
|       500       |        1         |           515200           |       97       |

**Suite 2 - 5 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|        5        |        1         |          20353344          |      1913      |
|        5        |        1         |          20353344          |      1873      |
|        5        |        1         |          20353344          |      1870      |
|        5        |        1         |          3377152           |      340       |
|        5        |        1         |          3377152           |      350       |
|        5        |        1         |          3377152           |      387       |
|        5        |        1         |           515200           |       72       |
|        5        |        1         |           515200           |       68       |
|        5        |        1         |           515200           |       71       |

**Suite 2 - 50 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|       50        |        1         |          20353344          |      1857      |
|       50        |        1         |          20353344          |      1781      |
|       50        |        1         |          20353344          |      1674      |
|       50        |        1         |          3377152           |      368       |
|       50        |        1         |          3377152           |      303       |
|       50        |        1         |          3377152           |      380       |
|       50        |        1         |           515200           |       45       |
|       50        |        1         |           515200           |       58       |
|       50        |        1         |           515200           |       52       |

**Suite 2 - 500 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|       500       |        1         |          20353344          |      1797      |
|       500       |        1         |          20353344          |      1808      |
|       500       |        1         |          20353344          |      1761      |
|       500       |        1         |          3377152           |      301       |
|       500       |        1         |          3377152           |      294       |
|       500       |        1         |          3377152           |      298       |
|       500       |        1         |           515200           |       60       |
|       500       |        1         |           515200           |       49       |
|       500       |        1         |           515200           |       53       |

**Suite 3 - 5 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|        5        |        1         |          20353344          |      1787      |
|        5        |        1         |          20353344          |      1793      |
|        5        |        1         |          20353344          |      1805      |
|        5        |        1         |          3377152           |      361       |
|        5        |        1         |          3377152           |      339       |
|        5        |        1         |          3377152           |      348       |
|        5        |        1         |           515200           |       74       |
|        5        |        1         |           515200           |       67       |
|        5        |        1         |           515200           |       71       |

**Suite 3 - 50 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|       50        |        1         |          20353344          |      1902      |
|       50        |        1         |          20353344          |      1885      |
|       50        |        1         |          20353344          |      1955      |
|       50        |        1         |          3377152           |      277       |
|       50        |        1         |          3377152           |      216       |
|       50        |        1         |          3377152           |      256       |
|       50        |        1         |           515200           |       64       |
|       50        |        1         |           515200           |       62       |
|       50        |        1         |           515200           |       65       |

**Suite 3 - 500 Partes**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|       500       |        1         |          20353344          |      1828      |
|       500       |        1         |          20353344          |      1911      |
|       500       |        1         |          20353344          |      2060      |
|       500       |        1         |          3377152           |      275       |
|       500       |        1         |          3377152           |      350       |
|       500       |        1         |          3377152           |      333       |
|       500       |        1         |           515200           |       62       |
|       500       |        1         |           515200           |       69       |
|       500       |        1         |           515200           |       65       |



Quanto ao filtro *CleanImage* os resultados obtidos foram:

**Suite 1**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|        5        |        2         |     12000000, 12000000     |      1236      |
|        5        |        2         |     12000000, 12000000     |      1077      |
|        5        |        2         |     12000000, 12000000     |      1177      |
|       50        |        2         |     12000000, 12000000     |      1198      |
|       50        |        2         |     12000000, 12000000     |      1071      |
|       50        |        2         |     12000000, 12000000     |      919       |
|       500       |        2         |     12000000, 12000000     |      1058      |
|       500       |        2         |     12000000, 12000000     |      970       |
|       500       |        2         |     12000000, 12000000     |      1015      |

| Number of Parts | Number of Images |  Number of Pixels per Image  | Execution Time |
| :-------------: | :--------------: | :--------------------------: | :------------: |
|        5        |        3         | 12000000, 12000000, 12000000 |      913       |
|        5        |        3         | 12000000, 12000000, 12000000 |      900       |
|        5        |        3         | 12000000, 12000000, 12000000 |      1005      |
|       50        |        3         | 12000000, 12000000, 12000000 |      1087      |
|       50        |        3         | 12000000, 12000000, 12000000 |      1078      |
|       50        |        3         | 12000000, 12000000, 12000000 |      1013      |
|       500       |        3         | 12000000, 12000000, 12000000 |      990       |
|       500       |        3         | 12000000, 12000000, 12000000 |      1055      |
|       500       |        3         | 12000000, 12000000, 12000000 |      1209      |

**Suite 2**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|        5        |        2         |     12000000, 12000000     |      867       |
|        5        |        2         |     12000000, 12000000     |      749       |
|        5        |        2         |     12000000, 12000000     |      839       |
|       50        |        2         |     12000000, 12000000     |      877       |
|       50        |        2         |     12000000, 12000000     |      831       |
|       50        |        2         |     12000000, 12000000     |      817       |
|       500       |        2         |     12000000, 12000000     |      819       |
|       500       |        2         |     12000000, 12000000     |      840       |
|       500       |        2         |     12000000, 12000000     |      821       |


| Number of Parts | Number of Images |  Number of Pixels per Image  | Execution Time |
| :-------------: | :--------------: | :--------------------------: | :------------: |
|        5        |        3         | 12000000, 12000000, 12000000 |      822       |
|        5        |        3         | 12000000, 12000000, 12000000 |      803       |
|        5        |        3         | 12000000, 12000000, 12000000 |      840       |
|       50        |        3         | 12000000, 12000000, 12000000 |      805       |
|       50        |        3         | 12000000, 12000000, 12000000 |      746       |
|       50        |        3         | 12000000, 12000000, 12000000 |      791       |
|       500       |        3         | 12000000, 12000000, 12000000 |      746       |
|       500       |        3         | 12000000, 12000000, 12000000 |      770       |
|       500       |        3         | 12000000, 12000000, 12000000 |      715       |

**Suite 3**

| Number of Parts | Number of Images | Number of Pixels per Image | Execution Time |
| :-------------: | :--------------: | :------------------------: | :------------: |
|        5        |        2         |     12000000, 12000000     |      1085      |
|        5        |        2         |     12000000, 12000000     |      974       |
|        5        |        2         |     12000000, 12000000     |      1019      |
|       50        |        2         |     12000000, 12000000     |      955       |
|       50        |        2         |     12000000, 12000000     |      1019      |
|       50        |        2         |     12000000, 12000000     |      971       |
|       500       |        2         |     12000000, 12000000     |      887       |
|       500       |        2         |     12000000, 12000000     |      914       |
|       500       |        2         |     12000000, 12000000     |      970       |


| Number of Parts | Number of Images |  Number of Pixels per Image  | Execution Time |
| :-------------: | :--------------: | :--------------------------: | :------------: |
|        5        |        3         | 12000000, 12000000, 12000000 |      888       |
|        5        |        3         | 12000000, 12000000, 12000000 |      988       |
|        5        |        3         | 12000000, 12000000, 12000000 |      911       |
|       50        |        3         | 12000000, 12000000, 12000000 |      821       |
|       50        |        3         | 12000000, 12000000, 12000000 |      897       |
|       50        |        3         | 12000000, 12000000, 12000000 |      871       |
|       500       |        3         | 12000000, 12000000, 12000000 |      882       |
|       500       |        3         | 12000000, 12000000, 12000000 |      880       |
|       500       |        3         | 12000000, 12000000, 12000000 |      859       |

### 3.2. Discussão dos resultados

De um modo geral, e como seria de esperar, independentemente do filtro e da abordagem executada, quanto maior a imagem maior é o tempo de execução.

Relativamente ao filtro *HighlightFires*, comprovou-se que o modo *Sequential* (média de execução igual de 2145 milissegundos para a maior imagem) é pior que o melhor modo *Multithreaded*, imagem dividida em 5 partes (média de execução igual de 1910 milissegundos para a maior imagem).

Quando se compara os resultados das diferentes *Suites* da abordagem Threadpool, comprova-se que, de uma modo geral, as melhores configurações foram as da *Suite* 2, como esperado.
Simultaneamente, é possível perceber que na *Suite* 3, a diferença de execução entre diferentes números de partes em que se divide a imagem não é significativa, o que também não se esperava, visto que, tendo o valor *KeepAliveTime* a 0, esperava-se que fossem criadas tantas *threads* quantas as partes que o utilizador requereu.
Isto porque, estando este valor a 0 significa que, após executar uma tarefa, a *thread* termina de imediato, não voltando para a *pool* e fazendo com que se tenha de criar mais *threads* (aumentando o tempo de execução). Assim, esperava-se que quanto maior o número de *threads*, maior o tempo de execução.

Já relativamente à *Suite* 2, esperava-se que esta fosse a melhor, visto que, o *KeepAliveTime* está a 60 segundos, o que significa que, após executada uma tarefa, a *thread* secundária irá esperar até um máximo de 60 segundos antes de terminar. Ora, estando o *corePoolSize* a 1 e o *maximumPoolSize* igual ao número de partes de divisão da imagem requeridas pelo utilizador, esperava-se que fossem criadas tantas *threads* quantas necessárias ao processamento. Porém, esperava-se que o número de *threads* criadas não fosse igual ao *maximumPoolSize*, uma vez que seriam reutilizadas *threads* e assim poupar-se-ia tempo na criação das mesmas.

Posto isto, quando se compara a melhor *Suite* da abordagem *Threadpool* com os resultados dos testes de *Multithreaded*, verificamos que existe uma melhoria dos tempos na abordagem Threadpool, tendo está uma média de 1694 milissegundos (Suite 2) para a maior imagem, contra 1910 milissegundos da *Multithreaded*.

Quanto ao Filtro de limpeza de imagens, também se comprovou que a abordagem *Multithreaded* (1179 milissegundos para processamento de 3 imagens e 5 *threads*) tem melhor desempenho que a *Sequential* (2083 milisegundos para processamento de 3 imagens).

Já para a abordagem *Threadpool* verificamos que a pior *Suite* de testes foi a 1, sendo esta a que apresenta mais *threads* criadas pois não existe reutilização, e é ainda possível validar que, ao contrário do que seria de esperar, os testes com 3 imagens obtiveram melhores tempos que os testes com 2 imagens.

Adicionalmente, tal como aconteceu com o filtro *HighlightFires*, de um modo geral, a melhor *Suite* de testes
foi a 2 (**corePoolSize** = 1; **maximumPoolSize** = número de partes requeridas pelo utilizador; **keepAliveTime** = 60 segundos) e 
para o maior número de threads (500 threads).

Assim sendo, para os dois filtros obtiveram-se os resultados esperados à priori.

## 4. Conclusão

Após a realização do presente trabalho, demonstrou-se que, nas implementações desenvolvidas dos filtros *HighlightFires* e *CleanImage*, a abordangem sequencial tem menor performance do que as abordagens *multithreaded* e baseada em *threadpool*.

Na abordagem *multithreaded*, observou-se que a performance diminui consoante o aumento do número de *threads* utilizadas no processamento e do número de imagens utilizadas, apenas no filtro *CleanImage*.

Por fim, relativamente à abordagem baseada em *threadpool*, considerou-se o melhor conjunto de configurações a Suite 2 em que se utilizou o arâmeto *corePoolSize* = 1, o parâmetro *maximumPoolSize* igual ao número de partes em que se divide a imagem requerido pelo utilizador e o parâmetro *keepAliveTime* igual a 60 segundos. Isto verificou-se em ambos os filtros.

Assim, concluímos que todos os objetivos do trabalho foram atingidos com sucesso.
